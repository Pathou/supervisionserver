# Créer une app serveur rapide avec lumen#


```
#!php

$app->get('/all', function () use ($app) {

  $token = $_GET['key'];
  $server = new \Carbone\SupervisionServer\SupervisionServer('$2y$10$HF2MPTgji9Dvq5OCfqcg1uoFwPisEGsPiZFC9zpBcAu14AdBoYhji'); // password_hash('pass', PASSWORD_DEFAULT);

  echo json_encode($server->handle_all($token));

});

$app->get('/ping', function () use ($app) {

  $token = $_GET['key'];
  $server = new \Carbone\SupervisionServer\SupervisionServer('$2y$10$HF2MPTgji9Dvq5OCfqcg1uoFwPisEGsPiZFC9zpBcAu14AdBoYhji'); // password_hash('pass', PASSWORD_DEFAULT);

  echo json_encode($server->handle_ping($token)); // 

});

```