<?php
namespace Carbone\SupervisionServer;

/**
 * Description of Supervision
 *
 * @author pdevalois
 */
class SupervisionServer
{
  
  /**
   * Clé de connection
   * @var type 
   */
  private $key;
  
  /**
   * @param string $key
   */
  public function __construct($key)
  {
    $this->key = $key;
  }
  
  /**
   * Retourne toute les infos
   * @param string $dist_key
   * @return array
   */
  public function handle_all($dist_key)
  {
    $this->_testConnection($dist_key);
    
    return [
      'ping' => $this->_ping(),
      'disk_space' => $this->_disk_space(),
      'ram' => $this->_ram(),
      'cpu' => $this->_cpu(),
      'uptime' => $this->_uptime()
    ];
  }
  
  /**
   * Répond un ping au serveur
   * @param string $dist_key
   * @return bool Ping Réussi ou non
   */
  public function handle_ping($dist_key) {
    
    $this->_testConnection($dist_key);
    
    return $this->_ping();
  }
  
  /**
   * retourne les infis du serveur
   * @param string $dist_key
   * @return array [uptime, idletime]
   */
  public function handle_infos($dist_key) {
    
    $this->_testConnection($dist_key);
    
    return $this->_infos();
  }
  
  /**
   * Retourne l'espace disque disponible
   * @param string $dist_key
   * @return array [libre, total]
   */
  public function handle_disk_space($dist_key, $path = '/') {
    
    $this->_testConnection($dist_key);
    
    return ['free' => disk_free_space($path), 'total' => disk_total_space($path)];
  }
  
  /**
   * Retourne le CPU
   * @param string $dist_key
   * @return array
   */
  public function handle_cpu($dist_key) {
    
    $this->_testConnection($dist_key);
    
    return $this->_cpu();
  }
  
  /**
   * Retourne la Ram
   * @param string $dist_key
   * @return array
   */
  public function handle_ram($dist_key) {
    
    $this->_testConnection($dist_key);
    
    return $this->_ram();
  }
  
  /**
   * Retourne l'uptime
   * @param string $dist_key
   * @return array
   */
  public function handle_uptime($dist_key) {
    
    $this->_testConnection($dist_key);
    
    return $this->_uptime();
  }

  /**
   * Test connection avec la base de données
   * @param string $dist_key
   * @param string $user
   * @param string $pass
   * @param string $host
   * @return array
   */
  public function handle_testdb($dist_key, $user, $pass, $host = 'localhost') {
    
    $this->_testConnection($dist_key);
    
    return $this->_connectiondatabase($user, $pass, $host);
  }
  
  /**
   * Envoie dump db
   * @param string $dist_key
   * @param string $user
   * @param string $pass
   * @param string $db
   * @param string $docker
   * @return array
   */
  public function handle_dumpdb($dist_key, $user, $pass, $db, $host = 'localhost', $do_with_php_lib = false) {
    
    $this->_testConnection($dist_key);
    
    return $this->_dumpdatabase($user, $pass, $db, $host, $do_with_php_lib);
  }

  /**
   * Lets connect !
   * @param string $dist_key
   * @return bool Clé ok ou non
   */
  private function _testConnection($dist_key) {
    
    if(!password_verify($dist_key, $this->key)) throw new BadKeyException('Bad Key');
  }
  
  /**
   * Retourne le ping
   * @return array
   */
  private function _ping() {
    $microtime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    
    return ['success' => true, 'time' => $microtime];
  }
  
  /**
   * Retourne les infos du serveur
   * @return array
   */
  private function _infos() {
    $os = shell_exec('lsb_release -is');
    $desc = shell_exec('lsb_release -ds');
    $version = shell_exec('lsb_release -rs');
    $name = shell_exec('lsb_release -cs');
    
    return ['os' => $os, 'desc' => $desc, 'version' => $version, 'name' => $name];
  }
  
  /**
   * Retourne le disk space
   * @return array
   */
  private function _disk_space($path = '/') {
    return ['free' => disk_free_space($path), 'total' => disk_total_space($path)];
  }
  
  /**
   * Retourne le CPU
   * @return array
   */
  private function _cpu() {
    /*$load = sys_getloadavg();
    return $load[0];*/
    $output = shell_exec('cat /proc/loadavg');
    $core_nums = trim(shell_exec("grep -P '^physical id' /proc/cpuinfo|wc -l"));
    $loadavg = substr($output,0,strpos($output," "));
    return ['cpu' => (float) $loadavg, 'nb' => (int) $core_nums];
  }
  
  /**
   * Retourne la Ram
   * array(43) {
      ["MemTotal"]=>
      string(10) "2060700 kB"
      ["MemFree"]=>
      string(9) "277344 kB"
      ["Buffers"]=>
      string(8) "92200 kB"
      ["Cached"]=>
      string(9) "650544 kB"
      ["SwapCached"]=>
      string(8) "73592 kB"
      ["Active"]=>
      string(9) "995988 kB"
      ...
	  en kB
   * @return array
   */
  private function _ram() {
    $data = explode("\n", file_get_contents("/proc/meminfo"));
    $meminfo = array();
    foreach ($data as $line) {
      @list($key, $val) = explode(":", $line);
      $meminfo[$key] = ((int) str_replace(['kB'], [''], $val) * 1024); // to octet
    }
    return $meminfo;
  }
  
  /**
   * Retourne le uptime
   * @return array
   */
  private function _uptime() {

    $output = shell_exec('cat /proc/uptime');
    list($uptime, $idle_time) = explode(' ', $output);
    
    return ['uptime' => (float) $uptime, 'idle_time' => (float) $idle_time];
  }
  
  /**
   * Retourne un dump de base de données
   * @param string $user
   * @param string $pass
   * @param string $host
   * @return array
   */
  private function _connectiondatabase($user, $pass, $host) {

    try {
      $connection = mysqli_connect($host, $user, $pass);
      $reason = false;

      if ($connection->connect_error) {
        $reason = $connection->connect_error;
      }
    }
    catch(\Exception $e) {
      $reason = $e->getMessage();
    }
    
    return ['connection' => $reason === false, 'reason' => $reason];
  }
  
  /**
   * Retourne un dump de base de données
   * @param string $user
   * @param string $pass
   * @param string $pass
   * @param string $db
   * @param string $do_with_php_lib
   * @return array
   */
  private function _dumpdatabase($user, $pass, $db, $host, $do_with_php_lib) {

    $mysql_dump = 'mysqldump --user='.$user.' --password='.$pass.' '.$db.' --single-transaction';
    //$sql = ($docker !== null ? `docker exec $docker bash -c '$mysql_dump'` : `$mysql_dump`);
    
	if(!$do_with_php_lib) {
		$sql = `$mysql_dump`;
	}
  // On utilise une lib PHP pour créer la backup
	else {
		$dump = new \MySQLDump(new \mysqli($host, $user, $pass, $db));
		
		ob_start();
		$dump->write();
		$sql = ob_get_contents();
		ob_end_clean();
	}
	
    return ['dump' => $sql];
  }
}
